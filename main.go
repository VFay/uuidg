package main

import (
	"flag"
	"fmt"
	"strings"

	"github.com/google/uuid"
)

func getUid(useUpper bool) string {
	if useUpper {
		return strings.ToUpper(uuid.New().String())
	} else {
		return uuid.New().String()
	}
}

func main() {
	var useUpper bool

	flag.BoolVar(&useUpper, "u", false, "")
	flag.Parse()

	uid := getUid(useUpper)

	fmt.Println(uid)
	fmt.Printf("\"%v\" \n", uid)
	fmt.Printf("{%v} \n", uid)
	fmt.Printf("new Guid(\"%v\") \n", uid)
	fmt.Printf("[Guid(\"%v\")] \n", uid)
}
